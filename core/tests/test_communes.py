from django.test import TestCase

from core.models import Departement


class TestDepartement(TestCase):
    def setUp(self):
        self.nom_departement = "Haut-de-Seine"

    def test_departement_a_le_bon_nom(self):
        # Given
        expected_name = self.nom_departement
        Departement.objects.create(nom=expected_name, code_departement=92)
        # When
        departement = Departement.objects.get(nom=expected_name)
        # Then
        self.assertEqual(departement.nom, expected_name)

    def test_departement_renvoie_un_html(self):
        # Given
        url = "/"
        # When
        response = self.client.get(url)
        # Then
        self.assertEqual(response.status_code, 200)
