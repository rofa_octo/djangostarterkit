from django.db import models


class Departement(models.Model):

    nom = models.CharField(max_length=250)
    code_departement = models.CharField(max_length=3)

    def __str__(self):
        return f"{self.nom}"
