from django.core.validators import RegexValidator
from django.db import models


class Commune(models.Model):

    class TypeCommune(models.TextChoices):
        VILLE = "VILLE", "Ville"  # VALEUR, label
        VILLAGE = "VILLAGE", "Village"
        LIEU_DIT = "LIEU_DIT", "Lieu-dit"

    nom = models.CharField(max_length=250)
    annee_de_creation = models.IntegerField(null=True, blank=True)
    code_postal = models.CharField(
        primary_key=True,
        max_length=5,
        validators=[
            RegexValidator(
                r"^(0[1-9]|[1-8]\d|9[0-5])\d{3}$ ",
                message="Veuillez rentrer un code postal valide."
            )
        ],
    )
    departement = models.ForeignKey(
        to="Departement",
        on_delete=models.CASCADE
    )
    type_commune = models.CharField(
        max_length=10,
        choices=TypeCommune.choices,
        default=TypeCommune.VILLE
    )

    def __str__(self):
        return f"{self.nom}"
