# Register your models here.

from django.contrib import admin
from .models.departement import Departement
from .models.commune import Commune

admin.site.register(Commune)
admin.site.register(Departement)