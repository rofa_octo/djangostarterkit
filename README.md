# DjangoStarterKit

This provides the basis of a Django project 

## Installation

### Prerequisites

If not already done, install python (version 3.8.5 [ici](https://www.python.org/downloads/release/python-383/))

Install pipenv, our environment manager with the following command: `pip3 install pipenv` (or `brew install pipenv` on MacOs)

### Quick install

Create your virtual environment: `pipenv install`

Activate your virtual environment: `pipenv shell`

Install the django library: `pipenv install django`

## launch application and make migrations
In the same shell : 
```python
python manage.py migrate
python manage.py loaddata departements
python manage.py loaddata communes
python manage.py runserver 
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Don't forget to add tests.